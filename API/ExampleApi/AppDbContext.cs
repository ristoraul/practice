﻿using ExampleApi.Model;
using Microsoft.EntityFrameworkCore;

namespace ExampleApi
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Curriculum> Curriculums { get; set; }

        public DbSet<Person> People { get; set; }

       


        protected override void OnModelCreating(ModelBuilder mb)
        {
            base.OnModelCreating(mb);

            mb.Entity<CourseCurriculum>().ToTable("CourseCurriculums")
                .HasKey(key => new {key.CourseId, key.CurriculumId});

            mb.Entity<StudentCourse>().ToTable("StudentCourses").HasKey(key => new {key.StudentId, key.CourseId});


            mb.Entity<Student>().ToTable("Students").HasKey(x => x.Id);
            mb.Entity<Person>().ToTable("People").HasKey(x => x.Id);

            mb.Entity<Student>().HasOne(x => x.Curriculum)
                .WithMany(x => x.StudentsInCurriculum)
                .HasForeignKey(x => x.CurriculumId);

            mb.Entity<Course>().ToTable("Courses").HasKey(x => x.Id);

            mb.Entity<Curriculum>().ToTable("Curriculums").HasKey(x => x.Id);


            mb.Entity<Course>().Property(x => x.Id).HasIdentityOptions(3);
            mb.Entity<Course>().HasData(
                new Course
                {
                    Id = 1, CourseCode = "ITB1704", Name = "Infosüsteemide arendamine IV: hajusrakendused (2020)"
                },
                new Course {Id = 2, CourseCode = "ITI0204", Name = "Algoritmid ja andmestruktuurid"}
            );

            mb.Entity<CourseCurriculum>().HasData(
                new CourseCurriculum() {CourseId = 1, CurriculumId = 1},
                new CourseCurriculum() {CourseId = 2, CurriculumId = 1}
            );

            mb.Entity<StudentCourse>().HasData(
                new StudentCourse() {CourseId = 1, StudentId = 1}
                );

            mb.Entity<Curriculum>().Property(x => x.Id).HasIdentityOptions(3);
            mb.Entity<Curriculum>().HasData(
                new Curriculum {Id = 1, CurriculumCode = "IABB", Name = "Äriinfotehnoloogia"},
                new Curriculum {Id = 2, CurriculumCode = "IVSB", Name = "Küberturbe tehnoloogiad"}
            );

            mb.Entity<Student>().Property(x => x.Id).HasIdentityOptions(3);
            mb.Entity<Student>().HasData(
                new Student {Id = 1, Name = "Gandalf", StudentCode = "IABB155344",CurriculumId = 1},
                new Student {Id = 2, Name = "Frodo", StudentCode = "IABB155423",CurriculumId = 1}
            );
            mb.Entity<Person>().Property(x => x.Id).HasIdentityOptions(3);
            mb.Entity<Person>().HasData(
                new Person { Id = 1, Name = "Risto", EMail = "ripaju@ttu.ee"}
            );


            mb.UseIdentityColumns();
        }
    }
}