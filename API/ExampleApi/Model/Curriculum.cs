﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ExampleApi.Model
{
    public class Curriculum
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string CurriculumCode { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<CourseCurriculum> CourseCurriculums { get; set; }
        [JsonIgnore]
        public virtual ICollection<Student> StudentsInCurriculum { get; set; }

    }
}
