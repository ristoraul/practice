﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ExampleApi.Model
{
    public class Student
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string StudentCode { get; set; }
        public string Name { get; set; }

        public int? CurriculumId { get; set; }

        [JsonIgnore]
        public virtual Curriculum Curriculum { get; set; }
        [JsonIgnore]

        public virtual ICollection<StudentCourse> StudentCourses { get; set; }

        public string Email
        {
            get => $"{StudentCode}@ttu.ee";
        }
    }

}
