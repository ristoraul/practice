﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ExampleApi.Model
{
    public class Course
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string CourseCode { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<StudentCourse> StudentCourses { get; set; }
        [JsonIgnore]
        public virtual ICollection<CourseCurriculum> CourseCurriculums { get; set; }

    }
}
