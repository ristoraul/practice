﻿using System.Text.Json.Serialization;

namespace ExampleApi.Model
{
    public class StudentCourse
    {
        public int StudentId { get; set; }
        public int CourseId { get; set; }

        [JsonIgnore]
        public virtual Student Student { get; set; }
        [JsonIgnore]
        public virtual Course Course { get; set; }
    }
}
