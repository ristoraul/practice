﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ExampleApi.Model
{
    public class CourseCurriculum
    {

        public int CourseId { get; set; }
        public int CurriculumId { get; set; }
       
        [JsonIgnore]
        public virtual Curriculum Curriculum { get; set; }
        [JsonIgnore]
        public virtual Course Course { get; set; }
    }
}
