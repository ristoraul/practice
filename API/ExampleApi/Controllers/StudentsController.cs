﻿using System;
using ExampleApi.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace ExampleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public StudentsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Students
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Student>>> GetStudents(string name = null, string code = null)
        {
            var query = _context.Students.AsQueryable();

            if (name != null)
                query = query.Where(x => x.Name.ToUpper().Contains(name.ToUpper()));

            if (code != null)
                query = query.Where(x => x.StudentCode.ToUpper().Contains(code.ToUpper()));
                
            return await query.ToListAsync();
        }

        // GET: api/Students/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Student>> GetStudent(int id)
        {
            var student = await _context.Students.FindAsync(id);

            if (student == null)
            {
                return NotFound();
            }

            return student;
        }

        // GET: api/Students/5/availablecourses
        [HttpGet("{id}/availablecourses")]
        public async Task<ActionResult<IEnumerable<Course>>> GetStudentAvailableCourses(int id)
        {
            //õpilane koos student-course seostega
            var student = _context.Students
                .Include(x=>x.StudentCourses)
                .First(x=>x.Id == id );

            //studentcourses seest kõik kursuste id'd
            var studentCourseIds = student.StudentCourses.Select(x => x.CourseId);

            //ainekava id
            var studentCurriculumId = student.CurriculumId;

            //ainekava koos course-curriculum ja course seostega
            var curriculum = _context.Curriculums
                .Include(x => x.CourseCurriculums)
                .ThenInclude(x=>x.Course)
                .First(x => x.Id == studentCurriculumId);

            //kursused kus õpilane pole veel registreeritud
            var notEnrolledCourses = _context.Courses.Where(
                x => !studentCourseIds.Contains(x.Id)).ToList();

            //ainekavas õpitavad kursused
            var potentialCurriculumCourses = 
                curriculum.CourseCurriculums.Select(x => x.Course).ToList();

            //kursused mis on ainekavas ning kuhu veel pole registreeritud
            var availablecourses = potentialCurriculumCourses.Where(x => notEnrolledCourses.Contains(x)).ToList();


            return availablecourses;
        }

        // GET: api/Students/5/courses
        [HttpGet("{id}/courses")]
        public async Task<ActionResult<IEnumerable<Course>>> GetStudentEnrolledCourses(int id)
        {
           var enrolledcourses =  _context.Students
               .Include(x=>x.StudentCourses).ThenInclude(x=>x.Course)
               .First(x=>x.Id == id ).StudentCourses.Select(x=>x.Course).ToList();
           return enrolledcourses;
        }

     

        // PUT: api/Students/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStudent(int id, Student student)
        {
            if (id != student.Id)
            {
                return BadRequest();
            }

            _context.Entry(student).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Students
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Student>> PostStudent(Student student)
        {
            _context.Students.Add(student);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetStudent", new { id = student.Id }, student);
        }


        // POST: api/Students/5/courses
        [HttpPost("{id}/courses")] 
        public async Task<ActionResult<StudentCourse>> AddStudentToCourse(int id, [FromBody] StudentCourse value)
        {
       
            var student = _context.Students.Include(x=>x.StudentCourses).FirstOrDefault(x=>x.Id == id);

            value.StudentId = id;

            if (student.StudentCourses == null)
                student.StudentCourses= new List<StudentCourse>();

            //taaskasutab teist requesti et näha kas on lubatud lisada
            var availableCourses = GetStudentAvailableCourses(id).Result.Value;

            if (availableCourses.Select(x => x.Id).Contains(value.CourseId))
            {
                student.StudentCourses.Add(value);
                await _context.SaveChangesAsync();
            }
            else
            {
                return Ok("Course not available");
            }
                


            return value;
        }

        // DELETE: api/Students/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Student>> DeleteStudent(int id)
        {
            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }

            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return student;
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.Id == id);
        }
    }
}
