﻿using ExampleApi.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurriculaController : ControllerBase
    {
        private readonly AppDbContext _context;

        public CurriculaController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Curricula
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Curriculum>>> GetCurriculums()
        {
            return await _context.Curriculums.ToListAsync();
        }

        // GET: api/Curricula/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Curriculum>> GetCurriculum(int id)
        {
            var curriculum = await _context.Curriculums.FindAsync(id);

            if (curriculum == null)
            {
                return NotFound();
            }

            return curriculum;
        }


        
        [HttpGet("{id}/courses")]
        public async Task<ActionResult<IEnumerable<Course>>> GetCoursesInCurriculum(int id)
        {
            var courses = await _context.Courses.Include(course => course.CourseCurriculums)
                .Where(course => course.CourseCurriculums.Select(x => x.CurriculumId).Contains(id)).ToListAsync();

            return courses;
        }

        // PUT: api/Curricula/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCurriculum(int id, Curriculum curriculum)
        {
            if (id != curriculum.Id)
            {
                return BadRequest();
            }

            _context.Entry(curriculum).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CurriculumExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Curricula
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Curriculum>> PostCurriculum(Curriculum curriculum)
        {
            _context.Curriculums.Add(curriculum);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCurriculum", new { id = curriculum.Id }, curriculum);
        }

        // DELETE: api/Curricula/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Curriculum>> DeleteCurriculum(int id)
        {
            var curriculum = await _context.Curriculums.FindAsync(id);
            if (curriculum == null)
            {
                return NotFound();
            }

            _context.Curriculums.Remove(curriculum);
            await _context.SaveChangesAsync();

            return curriculum;
        }

        private bool CurriculumExists(int id)
        {
            return _context.Curriculums.Any(e => e.Id == id);
        }
    }
}
