using ExampleApi.Model;
using NUnit.Framework;

namespace ExampleApi.Tests
{
    public class Tests
    {
        [Test]
        public void TestStudentEmail()
        {
            var student = new Student { Id = 1, Name = "Gandalf", StudentCode = "IABB155344", CurriculumId = 1 };

            Assert.AreEqual("IABB155344@ttu.ee", student.Email);
        }
    }
}