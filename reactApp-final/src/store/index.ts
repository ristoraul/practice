import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { studentReducer } from "../components/student/reducers/studentReducer";
import {curriculumReducer} from "../components/curriculum/reducers/curriculumReducer";
import {courseReducer} from "../components/course/reducers/courseReducer";
import {personReducer} from "../components/person/reducers/personReducer";

const rootReducer = combineReducers({
  student: studentReducer,
  curriculum: curriculumReducer,
  course: courseReducer,
  person: personReducer

});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
  const middlewares: any[] = [];
  const middleWareEnhancer = applyMiddleware(...middlewares);

  const store = createStore(
    rootReducer,
    composeWithDevTools(middleWareEnhancer)
  );

  return store;
}
