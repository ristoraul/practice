import React from "react";
import { Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Layout from "./layout/Layout";
import { StudentPage } from "./pages/studentPage";
import HomePage from "./pages/homePage";
import CurriculumPage from "./pages/curriculumPage";
import CoursePage from "./pages/coursePage";
import AddStudentForm from "./components/student/forms/addStudentForm";
import PersonPage from "./pages/personPage";

const App = () => {
  return (
    <div className="App">
      <Layout>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/student" exact component={StudentPage} />
          <Route path="/curriculum" exact component={CurriculumPage}/>
          <Route path="/course" exact component={CoursePage}/>
          <Route path="/test" exact component={AddStudentForm}/>
          <Route path="/person" exact component={PersonPage}/>
        </Switch>
      </Layout>
    </div>
  );
};

export default App;
