import axios from 'axios';
import {apiUrl} from "../../config/apiConfig";
import {FETCH_ALL_COURSES_ERROR, FETCH_ALL_COURSES_START, FETCH_ALL_COURSES_SUCCESS} from "./actions/courseActions";
import {Course} from "./courseTypes";


export const getAllCourses: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_ALL_COURSES_START());
    fetchAllCourses().then(data => dispatch(FETCH_ALL_COURSES_SUCCESS(data.data))).catch(err => dispatch(FETCH_ALL_COURSES_ERROR(err.message)))
}

export const fetchAllCourses = () => {
    return axios.get(`${apiUrl}/api/courses`);
};
console.log(getAllCourses)


export const addNewCourse = async (course: Course) => {
   return await postNewCourse(course);
};

const postNewCourse = (course: Course) => {
    console.log(JSON.stringify(course));
    return axios.post(`${apiUrl}/api/courses`, JSON.stringify(course), {headers: {"Content-Type": "application/json"}});
};


export const deleteCourseById = async (courseId: any) => {
    return await deleteCourse(courseId);
};


const deleteCourse = async (courseId: any) => {
    return axios.delete(`${apiUrl}/api/courses/${courseId}`);
};