export type Course = {
    id?: number,
    courseCode: string,
    name: string,
    curriculumId: any
}