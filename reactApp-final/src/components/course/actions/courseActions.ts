export const FETCH_ALL_COURSES_START = () => ({
    type: "FETCH_ALL_COURSES_START"
});

export const FETCH_ALL_COURSES_SUCCESS = (payload:any) => ({
    type: "FETCH_ALL_COURSES_SUCCESS",
    payload: payload
});

export const FETCH_ALL_COURSES_ERROR = (payload:any) => ({
    type: "FETCH_ALL_COURSES_ERROR",
    payload: payload
});