type Action = {
    type: any;
    payload: any;
};

type CourseReducerType = {
    allCourses: any[],
    coursesFetching: boolean,
    error: any
};

const initialState: CourseReducerType = {
    allCourses: [],
    coursesFetching: false,
    error: ""
};

export const courseReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case "FETCH_ALL_COURSES_START": {
            return {
                ...state,
                coursesFetching: true
            }
        }
        case "FETCH_ALL_COURSES_SUCCESS": {
            return {
                ...state,
                allCourses: action.payload,
                coursesFetching: false
            }
        }
        case "FETCH_ALL_COURSES_ERROR": {
            return {
                ...state,
                coursesFetching: false,
                error: action.payload
            }
        }
        default:
            return state;
    }
};
