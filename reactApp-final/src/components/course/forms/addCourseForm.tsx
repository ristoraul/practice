import React, {useState, useEffect} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {addNewCourse, getAllCourses} from "../courseQueries";
import {useDispatch, useSelector} from "react-redux";
import {getAllCurriculumsData} from "../../curriculum/curriculumQueries";
import {Curriculum} from "../../curriculum/curriculumTypes";
import {AppState} from "../../../store";

const AddCourseForm = () => {
    const dispatch = useDispatch();
    const [courseNameValue, setCourseNameValue] = useState<string>("");
    const [courseCodeValue, setCourseCodeValue] = useState<string>("");
    const [curriculumValue, setCurriculumValue] = useState<string>("");
    const [error, setError] = useState<string>("");

    const allCurriculums = useSelector<AppState, Curriculum[]>(state => state.curriculum.allCurriculums);

    useEffect(() => {
        //võtame kõikide curriculumide nimekirja
        getAllCurriculumsData(dispatch);
    }, []);
    //

    const handleCourseNameChange: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
        setCourseNameValue(e.target.value);
    };

    const handleCourseCodeChange: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
        setCourseCodeValue(e.target.value);
    };

    const handleCurriculumChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCurriculumValue(e.target.value);
    };

    const handleAddNewCourse = () => {
        setError("");
        if(courseNameValue !== "" && courseCodeValue !== "" && curriculumValue !== ""){
        addNewCourse({name: courseNameValue, courseCode: courseCodeValue, curriculumId: curriculumValue}).then(() => {setCourseNameValue("");
            setCourseCodeValue("");
            setCurriculumValue("");
            getAllCourses(dispatch);
        });

        }else {
            setError("Insufficient data")
        }
    };

    return (
        <Form>
            <FormGroup>
                <Label>Course Name</Label>
                <Input type="text" onChange={handleCourseNameChange} value={courseNameValue}/>
            </FormGroup>
            <FormGroup>
                <Label>Course Code</Label>
                <Input type="text" onChange={handleCourseCodeChange} value={courseCodeValue}/>
            </FormGroup>
            <FormGroup>
                <Label>Curriculum</Label>
                <Input type="select" value={curriculumValue} onChange={handleCurriculumChange}>
                    <option value=""/>
                    {allCurriculums?.map(curriculum => {
                        return <option value={curriculum.id}>{curriculum.name}</option>
                    })}
                </Input>
            </FormGroup>
            <FormGroup>
            <Button color="primary" onClick={() => handleAddNewCourse()}>ADD</Button>
            </FormGroup>
            {error ? <div className="error">{error}</div> : ""}

        </Form>
    )
}

export default AddCourseForm;