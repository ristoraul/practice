import React from 'react';
import {Course} from "./courseTypes";
import {Button, Table} from "reactstrap";
import {useDispatch} from "react-redux";

type TableProps = {
    data: Course[],
    canDelete: boolean,
    handleDelete: any
}

const CourseTable: React.FC<TableProps> = ({data, canDelete = false, handleDelete}) => {
    const dispatch = useDispatch();

    console.log(data);

    return (
        <Table>
            <thead>
            <tr>
            <th>
                Course ID
            </th>
            <th>Course</th>
            <th>Course Code</th>
                {canDelete ? <th/> : ""}
            </tr>
            </thead>
            <tbody>
                
            {data?.map(course => {
                return (
                    <tr key={"course" + course.id}>
                        <td>{course.id}</td>
                        <td>{course.name}</td>
                        <td>{course.courseCode}</td>
                        {canDelete ? <td><Button onClick={() => handleDelete(course.id)} color="danger">Delete</Button></td> : "" }
                    </tr>
                )
            })}
            </tbody>
        </Table>
    )
};

export default CourseTable;