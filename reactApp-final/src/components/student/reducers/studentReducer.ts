import {Course} from "../../course/courseTypes";
import {Student} from "../studentTypes";

type StudentReducerType = {
  selectedStudent: {};
  students: Student[];
  studentsFetching: boolean;
  coursesFetching: boolean;
  selectedStudentCourses: Course[];
  error: string;
};

const initialState: StudentReducerType = {
  selectedStudent: {},
  students: [],
  studentsFetching: false,
  coursesFetching: false,
  selectedStudentCourses: [],
  error: ""
};

export const studentReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "FETCH_STUDENTS_START": {
      return {
        ...state,
        studentsFetching: true
      };
    }
    case "FETCH_STUDENTS_SUCCESS": {
      return {
        ...state,
        studentsFetching: false,
        students: action.payload,
        selectedStudent: {}
      };
    }
    case "FETCH_STUDENTS_ERROR": {
      return {
        ...state,
        studentsFetching: false,
        error: action.payload
      };
    }
    case "SELECT_STUDENT": {
      return {
        ...state,
        selectedStudent: action.payload
      };
    }
    case "FETCH_STUDENT_COURSES_START": {
      return {
        ...state,
        coursesFetching: true
      }
    }
    case "FETCH_STUDENT_COURSES_SUCCESS": {
      return {
        ...state,
        coursesFetching: false,
        selectedStudentCourses: action.payload
      }
    }
    case "FETCH_STUDENT_COURSES_ERROR": {
      return {
        ...state
      }
    }
    default:
      return state;
  }
};
