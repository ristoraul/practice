import React, {useState} from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {Course} from "../../course/courseTypes";

const EnrollStudentToCourseForm: React.FC<{ data: Course[], handleCourseEnrollment: any }> = ({data, handleCourseEnrollment}) => {
    const [courseToAdd, setCourseToAdd] = useState("");

    const handleCourseSelectChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCourseToAdd(e.target.value);
    };


    return (
        <Form>
            <FormGroup>
                <Label for="availableCoursesSelect">Available Courses</Label>
                <Input type="select" value={courseToAdd} id="availableCoursesSelect"
                       onChange={handleCourseSelectChange}>
                    <option/>
                    {data.map(option => {
                        return (<option value={option.id}>{option.name}</option>)
                    })}
                </Input>
            </FormGroup>
            <Button disabled={courseToAdd === ""}
                    onClick={() => handleCourseEnrollment(courseToAdd, setCourseToAdd)}>Enroll</Button>
        </Form>
    )
};

export default EnrollStudentToCourseForm;