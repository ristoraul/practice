import React, { useState, useEffect } from "react";
import {useDispatch, useSelector} from "react-redux";
import { Label, Form, Input, FormGroup, Button } from "reactstrap";
import {addNewStudent, getAllStudents} from "../studentQueries";
import {Curriculum} from "../../curriculum/curriculumTypes";
import {getAllCurriculumsData} from "../../curriculum/curriculumQueries";
import {AppState} from "../../../store";

const AddStudentForm = () => {
  const [nameValue, setName] = useState("");
  const [codeValue, setCode] = useState("");
  const [formError, setFormError] = useState("");
  const [curriculumValue, setCurriculum] = useState();
  const dispatch = useDispatch();



  const allCurriculums = useSelector<AppState, Curriculum[]>(state => state.curriculum.allCurriculums);


  useEffect(() => {
      getAllCurriculumsData(dispatch);
  }, []);

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
  };

  const handleStudentCodeChange = (e:React.ChangeEvent<HTMLInputElement>) => {
      setCode(e.target.value);
  };

  const handleCurriculumChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurriculum(e.target.value);
  };

  const handleAddClick = () => {
    setFormError("");
    if (nameValue !== "" && curriculumValue !== "" && codeValue !== "") {
      addNewStudent(codeValue, nameValue, curriculumValue).then(() => getAllStudents(dispatch));
      setName("");
      setCode("");
      setCurriculum("");
    }
    else {
      setFormError("Insufficient data");
    }
  };

  return (
    <Form
      style={{
        margin: "10px 0px"
      }}
      onSubmit={e => e.preventDefault()}
    >
      <div>
        <FormGroup>
          <Label for="student_name">Name</Label>
          <Input
            type="text"
            name="name"
            value={nameValue}
            onChange={handleNameChange}
            id="student_name"
            placeholder="Student name"
        />
        </FormGroup>
        <FormGroup>
        <Label for="student_code">Student Code</Label>
          <Input
              type="text"
              name="name"
              value={codeValue}
              onChange={handleStudentCodeChange}
              id="student_code"
              placeholder="Student code"
          />
        </FormGroup>
        <FormGroup>
          <Label for="student__curriculum">Curriculum ID</Label>
          <Input
            type="select"
            name="curriculum"
            value={curriculumValue}
            onChange={handleCurriculumChange}
            id="student__curriculum"
            placeholder="Curriculum ID"
          >
            <option></option>
            {allCurriculums.map(curriculum => {
              return <option value={curriculum.id}>{curriculum.name}</option>
            } )}

          </Input>
        </FormGroup>
        {formError ? <p style={{ color: "red" }}>{formError}</p> : ""}
      </div>
      <Button color="primary" onClick={handleAddClick}>
        ADD
      </Button>
    </Form>
  );
};

export default AddStudentForm;
