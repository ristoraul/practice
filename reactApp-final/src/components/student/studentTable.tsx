import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Table} from "reactstrap";
import {SELECT_STUDENT} from "./actions/studentActions";
import {AppState} from "../../store";
import {Student} from "./studentTypes";
import {getCurriculumNameById} from "../curriculum/curriculumQueries";
import {deleteStudentById, getAllStudents} from "./studentQueries";

type TableProps = {
  data: Student[];
};

export const StudentTable = ({ data }: TableProps) => {
  const dispatch = useDispatch();
  const [students, setStudents] = useState<Student[]>([]);
  const selectedStudent = useSelector<AppState, Student>(
    state => state.student.selectedStudent
  );
  const handleStudentSelect = (student: {}) => {
    dispatch(SELECT_STUDENT(student));
  };


  const getStudentsWithCurriculumName = async (students: Student[]) => {
    if(students !== undefined && students.length){
    return await Promise.all(students.map(async (student: Student) => {
      return {
        ...student,
        curriculumName: await getCurriculumNameById(student.curriculumId)
      }
    }));}else{
      return Promise.resolve([]);
    }
  };

  //
  useEffect(() => {
    getStudentsWithCurriculumName(data).then(studentsWithCurriculum => setStudents(studentsWithCurriculum));
    console.log(data);
  }, [data]);

  const handleStudentDelete = (studentId: any) => {
    deleteStudentById(studentId).then(() => getAllStudents(dispatch));
  };


  useEffect(() => {
  }, [data]);

  return (
    <div className="student__table">
      <Table striped>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>EMail</th>
            <th>Curriculum</th>
            <th/>
          </tr>
        </thead>
          {students?.map((item: Student, idx: any) => {
            return (
              <tr
                key={idx}
                className="table-tr"
                id={"student" + item.id}
                onClick={() => handleStudentSelect(item)}
                style={{
                  backgroundColor:
                    item.id === selectedStudent.id ? "#5bc0de" : ""
                }}
              >
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.studentCode}</td>
                <td>{item.curriculumName}</td>
                <td><Button onClick={(e) => {e.stopPropagation(); handleStudentDelete(item.id)}} color="danger">Delete</Button></td>
              </tr>
            );
          })}
      </Table>
    </div>
  );
};
