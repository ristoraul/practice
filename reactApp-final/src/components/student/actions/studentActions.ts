export const FETCH_STUDENTS_START = () => ({
  type: "FETCH_STUDENTS_START" //START action ei saada andmeid, seega pole payload vajalik
});

export const FETCH_STUDENTS_SUCCESS = (payload: any) => ({
  type: "FETCH_STUDENTS_SUCCESS",
  payload: payload
});

export const FETCH_STUDENTS_ERROR = (payload: any) => ({
  type: "FETCH_STUDENTS_ERROR",
  payload: payload
});

export const SELECT_STUDENT = (payload: any) => ({
  type: "SELECT_STUDENT",
  payload: payload
});


export const  FETCH_STUDENT_COURSES_START = () => ({
  type: "FETCH_STUDENT_COURSES_START"
});

export const FETCH_STUDENT_COURSES_SUCCESS = (payload: any) => ({
  type: "FETCH_STUDENT_COURSES_SUCCESS",
  payload: payload
});

export const FETCH_STUDENT_COURSES_ERROR = (payload: any) => ({
  type: "FETCH_STUDENT_COURSES_ERROR",
  payload: payload
});
