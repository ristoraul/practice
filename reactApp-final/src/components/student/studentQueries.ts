import axios from 'axios' //axios on samuti hea 3rd party teek andmete pärimiseks
import {
    FETCH_STUDENT_COURSES_ERROR,
    FETCH_STUDENT_COURSES_START, FETCH_STUDENT_COURSES_SUCCESS,
    FETCH_STUDENTS_ERROR,
    FETCH_STUDENTS_START,
    FETCH_STUDENTS_SUCCESS
} from "./actions/studentActions";
import {apiUrl} from "../../config/apiConfig";


export const getAllStudents: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_STUDENTS_START());
    fetchAllStudents().then(data => dispatch(FETCH_STUDENTS_SUCCESS(data.data))).catch(err => dispatch(FETCH_STUDENTS_ERROR(err.message)))
};


export const fetchAllStudents = () => {
    return axios.get(`${apiUrl}/api/students`);
};

export const getStudentEnrolledCourses = (id: any, dispatch: any) => {
    dispatch(FETCH_STUDENT_COURSES_START());
    fetch(`${apiUrl}/api/students/${id}/courses`).then(response => response.json()).then(data => dispatch(FETCH_STUDENT_COURSES_SUCCESS(data))).catch(err => dispatch(FETCH_STUDENT_COURSES_ERROR(err)));
};

export const getStudentEnrollmentOptions = async (id: any) => {
    const data = await axios.get(`${apiUrl}/api/students/${id}/availablecourses`);
    return data.data;
};


export const addStudentToCourse = async (studentId: any, courseId: any) => {
    const body = {"StudentId": +studentId, "CourseId": +courseId};
    return axios.post(`${apiUrl}/api/students/${studentId}/courses`, body);
};

export const addNewStudent = async (studentCode: string, name: string, curriculumId: number) => {
    const studentToAdd = {"StudentCode": studentCode, "name": name, "curriculumId": +curriculumId};
    console.log(studentToAdd);
    return await axios.post(`${apiUrl}/api/students`, studentToAdd);
};

export const deleteStudentById = async (studentId: any) => {
    return await deleteStudent(studentId);
};


const deleteStudent = (studentId: any) => {
    return axios.delete(`${apiUrl}/api/students/${studentId}`);
};

