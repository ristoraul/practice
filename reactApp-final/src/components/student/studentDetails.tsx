import React, {useEffect, useState} from "react";
import {useSelector, useDispatch} from "react-redux";
import _ from 'lodash';
import {
    Card,
    CardBody,
    CardTitle,
    CardSubtitle,
    CardText,
    Button,
    Input
} from "reactstrap";
import {AppState} from "../../store";
import {Student} from "./studentTypes";
import {addStudentToCourse, getStudentEnrolledCourses, getStudentEnrollmentOptions} from "./studentQueries";
import CourseTable from "../course/courseTable";
import {Course} from "../course/courseTypes";
import EnrollStudentToCourseForm from "./forms/enrollStudentToCourseForm";
import {Curriculum} from "../curriculum/curriculumTypes";
import {fetchCurriculumById, getCurriculumNameById} from "../curriculum/curriculumQueries";

const StudentDetails: React.FC<{ data: Student }> = ({data}) => {
    const dispatch = useDispatch();

    const [availableCourses, setAvailableCourses] = useState<Course[]>([]);
    const selectedStudentCourses = useSelector<AppState, Course[]>(state => state.student.selectedStudentCourses);
    const [selectedStudentCurriculumData, setSelectedStudentCurriculumData] = useState<Curriculum>();

    const handleCourseEnrollment: (courseToAdd: string, setCourseToAdd: any) => void = (courseToAdd, setCourseToAdd) => {
        addStudentToCourse(data.id, courseToAdd).then(() => getStudentEnrolledCourses(data.id, dispatch));
        setCourseToAdd("");
    };


    useEffect(() => {
        if (!_.isEmpty(data)) { //kontrollime, et Student objekt ei oleks tühi
            setAvailableCourses([]);
            getStudentEnrolledCourses(data.id, dispatch);
            fetchCurriculumById(data.curriculumId).then(d => setSelectedStudentCurriculumData(d.data));
            getStudentEnrollmentOptions(data.id).then(data => setAvailableCourses(data));
            getCurriculumNameById(1).then(d => console.log(d));
        }

    }, [data]);


        return (
            <div>
                <Card>
                    <CardBody>
                        <CardTitle style={{fontSize: "20px", fontWeight: "bold"}}>{data.name}</CardTitle>
                        <p><span style={{fontWeight: "bold"}}>Student Code: </span>{data.studentCode}</p>
                        <CardSubtitle style={{fontWeight: "bold"}}>Curriculum</CardSubtitle>
                        <CardText>{selectedStudentCurriculumData?.name}</CardText>
                        <CardSubtitle style={{fontWeight: "bold"}}>
                            Enrolled courses
                        </CardSubtitle>
                        <CardText>
                                <p>Student is not enrolled in any course</p>

                        </CardText>
                        <EnrollStudentToCourseForm data={availableCourses}
                                                   handleCourseEnrollment={handleCourseEnrollment}/>
                    </CardBody>
                </Card>
            </div>
        );

};

export default StudentDetails;
