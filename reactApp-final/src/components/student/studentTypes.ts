export type Student = {
    id: number,
    name: string,
    curriculumId: any,
    studentCode: any,
    curriculumName?:string
}