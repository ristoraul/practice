import { Person } from "../personTypes";

export const FETCH_ALL_PEOPLE_START = () => ({
    type: "FETCH_ALL_PEOPLE_START" //START action ei saada andmeid, seega pole payload vajalik
  });
  
  export const FETCH_ALL_PEOPLE_SUCCESS = (payload: any) => ({
    type: "FETCH_ALL_PEOPLE_SUCCESS",
    payload: payload
  });
  
  export const FETCH_ALL_PEOPLE_ERROR = (payload: any) => ({
    type: "FETCH_ALL_PEOPLE_ERROR",
    payload: payload
  });
  
  export const SELECT_PERSON = (payload: Person) => ({
    type: "SELECT_PERSON",
    payload: payload
  })
  
  
  /*export const  FETCH_STUDENT_COURSES_START = () => ({
    type: "FETCH_STUDENT_COURSES_START"
  });
  
  export const FETCH_STUDENT_COURSES_SUCCESS = (payload: any) => ({
    type: "FETCH_STUDENT_COURSES_SUCCESS",
    payload: payload
  });
  
  export const FETCH_STUDENT_COURSES_ERROR = (payload: any) => ({
    type: "FETCH_STUDENT_COURSES_ERROR",
    payload: payload
  });*/
  