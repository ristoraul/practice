import { Person } from "../personTypes";

type Action = {
  type: any;
  payload: any;
};

type PersonReducerType = {
  //selectedPerson: {};
  people: any[];
  peopleFetching: boolean;
  selectedPerson: Person;
  error: any;
};

const initialState: PersonReducerType = {
 // selectedPerson: {},
  people: [],
  selectedPerson: {} as Person,
  peopleFetching: false,
  error: ""
};

export const personReducer = (state = initialState, action: Action) => {
  switch (action.type) {
    case "FETCH_ALL_PEOPLE_START": {
      return {
        ...state,
        peopleFetching: true
      };
    }
    case "FETCH_ALL_PEOPLE_SUCCESS": {
      return {
        ...state,
        people: action.payload,
        peopleFetching: false
   //     selectedPerson: {}
      };
    }
    case "FETCH_ALL_PEOPLE_ERROR": {
      return {
        ...state,
        peopleFetching: false,
        error: action.payload
      };
    }
    case "SELECT_PERSON": {
      return {
        ...state,
        selectedPerson: action.payload
      };
    }
    default:
      return state;
  }
};
