import React, {useEffect, useState} from "react";
import {Card, CardBody, CardSubtitle, CardText, CardTitle} from "reactstrap";
import {Person} from "./personTypes";
import {getPersonNameById} from "./personQueries";

const PersonDetails: React.FC<{selectedPerson: Person}> = ({selectedPerson}) => {
  
  useEffect(() => {
    
  }, [selectedPerson]);


  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle>
            <span style={{ fontWeight: "bold", fontSize: "15px" }}>
              {selectedPerson.name}
            </span>
          </CardTitle>
          <CardSubtitle>EMail</CardSubtitle>
          <CardText><span style={{fontWeight: 'bold'}}>{selectedPerson.eMail}</span></CardText>
          <CardSubtitle>ID</CardSubtitle>
          <CardText><span style={{fontWeight: 'bold'}}>{selectedPerson.id}</span></CardText>
         
        </CardBody>
      </Card>
    </div>
  );
};

export default PersonDetails;
