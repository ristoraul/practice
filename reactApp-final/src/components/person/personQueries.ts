import axios from 'axios' //axios on samuti hea 3rd party teek andmete pärimiseks
import {
    FETCH_ALL_PEOPLE_ERROR,
    FETCH_ALL_PEOPLE_START,
    FETCH_ALL_PEOPLE_SUCCESS
} from "./actions/personActions";
import {apiUrl} from "../../config/apiConfig";
import { Person } from './personTypes';


export const getAllPeople: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_ALL_PEOPLE_START());
    fetchAllPeople().then(data => dispatch(FETCH_ALL_PEOPLE_SUCCESS(data.data))).catch(err => dispatch(FETCH_ALL_PEOPLE_ERROR(err.message)))
};

export const fetchAllPeople = () => {
    return axios.get(`${apiUrl}/api/people`);
   
};
console.log(getAllPeople)

export const getPersonNameById = async (id: number) => {
    if(id){
    let personName = await fetchPersonById(id);
    return personName.data.name;}else {
        return ""
    }
};

export const fetchPersonById = (id: number) => {
    return axios.get(`${apiUrl}/api/people/${id}`);
};

export const addNewPerson = async (person: Person) => {
    return await postNewPerson(person);
 };
 
 const postNewPerson = (person: Person) => {
     console.log(JSON.stringify(person));
     return axios.post(`${apiUrl}/api/people`, JSON.stringify(person), {headers: {"Content-Type": "application/json"}});
 };

export const deletePersonById = async (personId: any) => {
    return await deletePerson(personId);
};


const deletePerson = (personId: any) => {
    return axios.delete(`${apiUrl}/api/people/${personId}`);
};

