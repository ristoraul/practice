import React from "react";
import {useDispatch} from "react-redux";
import {Button, Table} from "reactstrap";
import {Person} from "./personTypes";
import { deletePersonById, getAllPeople } from "./personQueries";
import { SELECT_PERSON } from "./actions/personActions";
import {useSelector} from "react-redux";
import { AppState } from "../../store";


type TableProps = {
  data: Person[],
  canDelete: boolean,
  handleDelete:any
};

const PersonTable: React.FC<TableProps> = ({ data, canDelete = false, handleDelete}) => {

  const dispatch = useDispatch();
  const selectedPerson = useSelector<AppState, Person>(state=> state.person.selectedPerson)
  const state = [
    {id: 1, name: "Risto", eMail: "ripaju@ttu.ee"}
  ]

  const handlePersonSelect: any = (person: any) => {
    dispatch(SELECT_PERSON(person));
};
  console.log(data);
  return (
      
    <Table>
        <thead>
        <tr>
        <th>
            Person ID
        </th>
        <th>Name</th>
        <th>EMail</th>
            {canDelete ? <th/> : ""}
        </tr>
        </thead>
        <tbody>
            
        {data.map(person => {
            return (
                
            <tr key={person.id} className="table-tr" onClick={() => handlePersonSelect(person)} style={{backgroundColor: selectedPerson.id === person.id ? "#5bc0de" : ""}} >
                    
                    <td>{person.id}</td>
                    <td>{person.name}</td>
                    <td>{person.eMail}</td>
                    {canDelete ? <td><Button onClick={() => handleDelete(person.id)} color="danger">Delete</Button></td> : "" }
                </tr>
            )
        })}
        </tbody>
    </Table>
);
};
export default PersonTable;
