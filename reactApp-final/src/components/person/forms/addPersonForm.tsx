import React, {useState, useEffect} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {addNewPerson, getAllPeople} from "../personQueries";
import {useDispatch, useSelector} from "react-redux";
import {getAllCurriculumsData} from "../../curriculum/curriculumQueries";
import {Curriculum} from "../../curriculum/curriculumTypes";
import {AppState} from "../../../store";

const AddPersonForm = () => {
    const dispatch = useDispatch();
    const [personNameValue, setPersonNameValue] = useState<string>("");
    const [personEMailValue, setPersonEMailValue] = useState<string>("");
    const [error, setError] = useState<string>("");


  

    const handlePersonNameChange: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
        setPersonNameValue(e.target.value);
    };

    const handlePersonEMailChange: (e: React.ChangeEvent<HTMLInputElement>) => void = (e) => {
        setPersonEMailValue(e.target.value);
    };


    const handleAddNewPerson = () => {
        setError("");
        if(personNameValue !== "" && personEMailValue !== ""){
        addNewPerson({name: personNameValue, eMail: personEMailValue}).then(() => {setPersonNameValue("");
            setPersonEMailValue("");
            getAllPeople(dispatch);
        });

        }else {
            setError("Insufficient data")
        }
    };

    return (
        <Form>
            <FormGroup>
                <Label>Person Name</Label>
                <Input type="text" onChange={handlePersonNameChange} value={personNameValue}/>
            </FormGroup>
            <FormGroup>
                <Label>Person EMail</Label>
                <Input type="text" onChange={handlePersonEMailChange} value={personEMailValue}/>
            </FormGroup>
            <FormGroup>
            </FormGroup>
            <FormGroup>
            <Button color="primary" onClick={() => handleAddNewPerson()}>ADD</Button>
            </FormGroup>
            {error ? <div className="error">{error}</div> : ""}

        </Form>
    )
}

export default AddPersonForm;