import React, {useState} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {addNewCurriculum, getAllCurriculumsData} from "../curriculumQueries";
import {useDispatch} from "react-redux";


const AddCurriculumForm = () => {
    const dispatch = useDispatch();
    const [curriculumNameValue, setCurriculumNameValue] = useState<string>("");
    const [curriculumCodeValue, setCurriculumCodeValue] = useState<string>("");
    const [error, setError] = useState<string>("");

    const handleCurriculumNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCurriculumNameValue(e.target.value);
    };

    const handleCurriculumCodeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCurriculumCodeValue(e.target.value);
    };

    const handleAddCurriculum = () => {
        setError("");
        if(curriculumCodeValue !== "" && curriculumNameValue !== ""){
        addNewCurriculum({name: curriculumNameValue, curriculumCode: curriculumCodeValue}).then(() => {setCurriculumCodeValue("");
        setCurriculumNameValue("");
        getAllCurriculumsData(dispatch)
        })} else{
            setError("Insufficient data");
        }
    };

    return (
        <Form onSubmit={e => e.preventDefault()}>
            <FormGroup>
                <Label>Curriculum Name</Label>
                <Input type="text" onChange={handleCurriculumNameChange} value={curriculumNameValue} id="addCurriculumName"/>
            </FormGroup>
            <FormGroup>
                <Label>Curriculum Code</Label>
                <Input type="text" onChange={handleCurriculumCodeChange} value={curriculumCodeValue} id="addCurriculumCode"/>
            </FormGroup>
            <FormGroup>
                <Button onClick={handleAddCurriculum} color="primary">ADD</Button>
            </FormGroup>
            {error ? <div className="error">{error}</div> : ""}
        </Form>
    )
}

export default AddCurriculumForm;