import React from "react";
import {Button, Table} from "reactstrap";
import {Curriculum} from "./curriculumTypes";
import {useDispatch, useSelector} from "react-redux";
import {deleteCurriculumById, getAllCurriculumsData} from "./curriculumQueries";
import {SELECT_CURRICULUM} from "./actions/curriculumActions";
import {AppState} from "../../store";

type CurriculumTableProps = {
    data: Curriculum[],
    canDelete: boolean
}

const CurriculumTable:React.FC<CurriculumTableProps> = ({data, canDelete = false}) => {
    const dispatch = useDispatch();
    const selectedCurriculum = useSelector<AppState, Curriculum>(state => state.curriculum.selectedCurriculum);

    const handleCurriculumSelect: any = (curriculum: any) => {
        dispatch(SELECT_CURRICULUM(curriculum));
    };

    const handleCurriculumDelete = (curriculumId: any) => {
        deleteCurriculumById(curriculumId).then(() => {
            getAllCurriculumsData(dispatch);
        })
    };


    return (
        <Table striped>
            <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Curriculum Code</th>
            {canDelete ? <th/> : ""}
            </thead>
            <tbody>
            {data?.map(curriculum => {
                return (
                    <tr key={curriculum.id} onClick={() => handleCurriculumSelect(curriculum)} style={{backgroundColor: selectedCurriculum.id === curriculum.id ? "#5bc0de" : ""}}>
                        <td>{curriculum.id}</td>
                        <td>{curriculum.name}</td>
                        <td>{curriculum.curriculumCode}</td>
                        {canDelete ? <td><Button color="danger" onClick={() => handleCurriculumDelete(curriculum.id)}>Delete</Button></td> : ""}
                    </tr>
                )
            })}
            </tbody>
        </Table>
    )

};

export default CurriculumTable