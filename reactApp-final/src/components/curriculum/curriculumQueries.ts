import axios from 'axios';
import {apiUrl} from "../../config/apiConfig";
import {FETCH_STUDENT_COURSES_ERROR, FETCH_STUDENT_COURSES_START} from "../student/actions/studentActions";
import {FETCH_ALL_CURRICULUMS_SUCCESS} from "./actions/curriculumActions";
import {Curriculum} from "./curriculumTypes";


export const getAllCurriculumsData: (dispatch: any) => void = (dispatch) => {
    dispatch(FETCH_STUDENT_COURSES_START());
    fetchAllCurriculums().then(data => dispatch(FETCH_ALL_CURRICULUMS_SUCCESS(data.data))).catch(err => dispatch(FETCH_STUDENT_COURSES_ERROR(err.message)));
};

export const fetchAllCurriculums = () => {
    return axios.get(`${apiUrl}/api/curricula`);
};



export const getCurriculumNameById = async (id: number) => {
    if(id){
    let curriculumName = await fetchCurriculumById(id);
    return curriculumName.data.name;}else {
        return ""
    }
};

export const fetchCurriculumById = (id: number) => {
    return axios.get(`${apiUrl}/api/curricula/${id}`);
};

export const addNewCurriculum = async (curriculum: Curriculum) => {
    return await postNewCurriculum(curriculum);
};


export const postNewCurriculum = (curriculum: Curriculum) => {
    console.log(JSON.stringify(curriculum));
    return axios.post(`${apiUrl}/api/curricula`, JSON.stringify(curriculum), {headers: {"Content-Type": "application/json"}});
};


export const deleteCurriculumById = async (courseId: any) => {
    return await deleteCurriculum(courseId);
};


const deleteCurriculum = async (curriculumId: any) => {
    return axios.delete(`${apiUrl}/api/curricula/${curriculumId}`);
};


export const getCurriculumCourses = async (curriculumId: any) => {
   return await fetchCurriculumCourses(curriculumId);
};

const fetchCurriculumCourses = (curriculumId: any) => {
    return axios.get(`${apiUrl}/api/curricula/${curriculumId}/courses`)
};