import {Curriculum} from "../curriculumTypes";

export const FETCH_ALL_CURRICULUMS_START = () => ({
    type: "FETCH_ALL_CURRICULUMS_START"
});

export const FETCH_ALL_CURRICULUMS_SUCCESS = (payload: any) => ({
    type: "FETCH_ALL_CURRICULUMS_SUCCESS",
    payload: payload
});

export const FETCH_ALL_CURRICULUMS_ERROR = (payload: any) => ({
    type: "FETCH_ALL_CURRICULUMS_ERROR",
    payload: payload
});


export const SELECT_CURRICULUM = (payload: Curriculum) => ({
    type: "SELECT_CURRICULUM",
    payload: payload
});