import {Curriculum} from "../curriculumTypes";

type Action = {
    type: any;
    payload: any;
};

type CurriculumReducerType = {
    allCurriculums: any[],
    curriculumsFetching: boolean,
    selectedCurriculum: Curriculum,
    error: any
};

const initialState: CurriculumReducerType = {
    allCurriculums: [],
    selectedCurriculum: {} as Curriculum,
    curriculumsFetching: false,
    error: ""
};

export const curriculumReducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case "FETCH_ALL_CURRICULUMS_START": {
            return {
                ...state,
                curriculumsFetching: true
            }
        }
        case "FETCH_ALL_CURRICULUMS_SUCCESS": {
            return {
                ...state,
                allCurriculums: action.payload,
                curriculumsFetching: false
            }
        }
        case "FETCH_ALL_CURRICULUMS_ERROR": {
            return {
                ...state,
                curriculumsFetching: false,
                error: action.payload

            }
        }
        case "SELECT_CURRICULUM": {
            return {
                ...state,
                selectedCurriculum: action.payload
            }
        }
        default:
            return state;
    }
};
