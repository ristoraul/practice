export type Curriculum = {
    id?: number,
    name: string,
    curriculumCode: any
}