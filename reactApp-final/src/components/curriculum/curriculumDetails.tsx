import React, {useEffect, useState} from "react";
import {Card, CardBody, CardSubtitle, CardText, CardTitle} from "reactstrap";
import {Curriculum} from "./curriculumTypes";
import CourseTable from "../course/courseTable";
import {getCurriculumCourses, getCurriculumNameById} from "./curriculumQueries";

const CurriculumDetails: React.FC<{selectedCurriculum: Curriculum}> = ({selectedCurriculum}) => {
  const [curriculumCourses, setCurriculumCourses] = useState([]);
  useEffect(() => {
    getCurriculumCourses(selectedCurriculum.id).then(d => setCurriculumCourses(d.data));
  }, [selectedCurriculum]);


  return (
    <div>
      <Card>
        <CardBody>
          <CardTitle>
            <span style={{ fontWeight: "bold", fontSize: "15px" }}>
              {selectedCurriculum.name}
            </span>
          </CardTitle>
          <CardSubtitle>Code</CardSubtitle>
          <CardText><span style={{fontWeight: 'bold'}}>{selectedCurriculum.curriculumCode}</span></CardText>
          <CardSubtitle>ID</CardSubtitle>
          <CardText><span style={{fontWeight: 'bold'}}>{selectedCurriculum.id}</span></CardText>
          <CardTitle>Courses in curriculum</CardTitle>
          <CourseTable data={curriculumCourses} canDelete={false} handleDelete={() => {}}/>
        </CardBody>
      </Card>
    </div>
  );
};

export default CurriculumDetails;
