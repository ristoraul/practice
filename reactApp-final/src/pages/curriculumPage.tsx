import React, {useEffect, useState} from 'react';
import _ from 'lodash';
import AddCurriculumForm from "../components/curriculum/forms/addCurriculumForm";
import CurriculumTable from "../components/curriculum/curriculumTable";
import {useDispatch, useSelector} from "react-redux";
import {getAllCurriculumsData} from "../components/curriculum/curriculumQueries";
import {Curriculum} from "../components/curriculum/curriculumTypes";
import {AppState} from "../store";
import CurriculumDetails from "../components/curriculum/curriculumDetails";
import {Button, Collapse} from "reactstrap";

const CurriculumPage = () => {
    const dispatch = useDispatch();
    const [addCurriculumFormOpen, setAddCurriculumFormOpen] = useState<boolean>(false);
    const selectedCurriculum = useSelector<AppState, Curriculum>(
        state => state.curriculum.selectedCurriculum
    );

    const handleToggleAddCurriculumForm = () => {
        setAddCurriculumFormOpen(state => !state);
    };


    const allCurriculums = useSelector<AppState, Curriculum[]>(state => state.curriculum.allCurriculums);
    useEffect(() => {
        getAllCurriculumsData(dispatch);
    }, []);

return (
    <React.Fragment>
        <Button onClick={handleToggleAddCurriculumForm} style={{margin: "15px 0px"}} color={addCurriculumFormOpen ? "danger" : "primary"}>{addCurriculumFormOpen ? "Close" : "Add a curriculum"}</Button>
        <Collapse isOpen={addCurriculumFormOpen}>
        <h2>Add a Curriculum</h2>
        <AddCurriculumForm/>
        </Collapse>

        <h4>Curriculums</h4>
        <CurriculumTable canDelete={true} data={allCurriculums}/>
        {!_.isEmpty(selectedCurriculum) ?
        <CurriculumDetails selectedCurriculum={selectedCurriculum}/> : ""}
    </React.Fragment>
    )
};

export default CurriculumPage;