import React, {useEffect, useState} from 'react';
import AddCourseForm from "../components/course/forms/addCourseForm";
import CourseTable from "../components/course/courseTable";
import {deleteCourseById, getAllCourses} from "../components/course/courseQueries";
import {useDispatch, useSelector} from "react-redux";
import {AppState} from "../store";
import {Course} from "../components/course/courseTypes";
import Loading from "../components/common/loading";
import {Button, Collapse} from "reactstrap";

const CoursePage = () => {
    const dispatch = useDispatch();
    const [addCourseFormOpen, setAddCourseFormOpen] = useState<boolean>(false);
    const allCourses = useSelector<AppState, Course[]>(state => state.course.allCourses);
    const coursesFetching = useSelector<AppState, boolean>(state => state.course.coursesFetching);
    const handleToggleCourseFormOpen = () => {
        setAddCourseFormOpen(state => !state);
    };

    const handleCourseDelete = (id: any) => {
        deleteCourseById(id).then(() => getAllCourses(dispatch));
    };

    useEffect(() => {
        getAllCourses(dispatch);
    }, []);

    return (
        <React.Fragment>
            <Button onClick={handleToggleCourseFormOpen} style={{margin: "15px 0px"}} color={addCourseFormOpen ? "danger" : "primary"}>{addCourseFormOpen ? "Close" : "Add a course"}</Button>
            <Collapse isOpen={addCourseFormOpen}>
            <h4>Add a Course</h4>
            <AddCourseForm/>
            </Collapse>
            <h4>Courses</h4>
            {!coursesFetching ? <CourseTable canDelete={true} data={allCourses} handleDelete={handleCourseDelete}/> : <Loading/>}

        </React.Fragment>
    )

};

export default CoursePage;