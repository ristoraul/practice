import React, {useEffect, useState} from 'react';
import PersonTable from "../components/person/personTable";
import {useDispatch, useSelector} from "react-redux";
import {getAllPeople, deletePersonById} from "../components/person/personQueries";
import {Person} from "../components/person/personTypes";
import {AppState} from "../store";
import {Button, Collapse} from "reactstrap";
import Loading from '../components/common/loading';
import AddPersonForm from '../components/person/forms/addPersonForm';
import PersonDetails from '../components/person/personDetails';
import _ from 'lodash';

const PersonPage = () => {
    const dispatch = useDispatch();
    const [addPersonFormOpen, setAddPersonFormOpen] = useState<boolean>(false);
    const people = useSelector<AppState, Person[]>(state => state.person.people);
    const peopleFetching = useSelector<AppState, boolean>(state => state.person.peopleFetching);
    const selectedPerson = useSelector<AppState, Person>(
        state => state.person.selectedPerson
    );
   
    const handleToggleAddPersonForm = () => {
        setAddPersonFormOpen(state => !state);
    };

    const handlePersonDelete = (id: any) => {
      deletePersonById(id).then(() => getAllPeople(dispatch));
  };
  

   // const selectedPerson = useSelector<AppState, Person>(state => state.person.selectedPerson);
    useEffect(() => {
        getAllPeople(dispatch);
    }, []);

return (
    <React.Fragment>
        <Button onClick={handleToggleAddPersonForm} style={{margin: "15px 0px"}} color={addPersonFormOpen ? "danger" : "primary"}>{addPersonFormOpen ? "Close" : "Add a person"}</Button>
        <Collapse isOpen={addPersonFormOpen}>
        <h4>Add a Person</h4>
        <AddPersonForm/>
        </Collapse>
        <h4>People</h4>
        {!peopleFetching ? <PersonTable canDelete={true} data={people} handleDelete ={handlePersonDelete}/> : <Loading/>}
        {!_.isEmpty(selectedPerson) ?
        <PersonDetails selectedPerson={selectedPerson}/> : ""}

    </React.Fragment>
    )
};

export default PersonPage;