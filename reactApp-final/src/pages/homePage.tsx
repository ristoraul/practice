import React from "react";

const HomePage = () => {
  return (
    <div>
      <div className="page__heading">Home</div>
      <p>Hajusrakendused ITB1704 demo rakendust, mis demonstreerib API kasutamist Reactiga</p>
    </div>
  );
};

export default HomePage;
