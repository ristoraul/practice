import React, {useEffect, useState} from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { StudentTable } from "../components/student/studentTable";
import { AppState } from "../store";
import StudentDetails from "../components/student/studentDetails";
import AddStudentForm from "../components/student/forms/addStudentForm";
import {getAllStudents} from "../components/student/studentQueries";
import {Student} from "../components/student/studentTypes";
import {Button, Collapse} from "reactstrap";

export const StudentPage = () => {
  const dispatch = useDispatch(); //Annab meile dispatch instance'i, mis on vajalik actionite saatmiseks reduceritesse
    const [addFormOpen, setAddFormOpen] = useState<boolean>(false);

    const toggleAddStudentFormOpen = () => {
        setAddFormOpen(state => !state);
    };


  const students = useSelector<AppState, any[]>(
    state => state.student.students
  ); //useSelector võimaldab valida andmeid Redux state'ist

  const selectedStudent = useSelector<AppState, Student>(
    state => state.student.selectedStudent
  );

  useEffect(() => {
    //useEffect hook on sisuliselt sama, mis class componentides on componentDidMount/componentDidUpdate
    getAllStudents(dispatch);
  }, []);

  return (
    <React.Fragment>
        <Button color={addFormOpen ? "danger" : "primary"} style={{margin: "15px 0px"}} onClick={toggleAddStudentFormOpen}>{addFormOpen ? "Close" : "Add a student"}</Button>
        <Collapse isOpen={addFormOpen}>
      <h2>Add a Student</h2>
      <AddStudentForm />
        </Collapse>

        <h4>Students</h4>
      <StudentTable data={students} />
        {!_.isEmpty(selectedStudent) ? <StudentDetails data={selectedStudent}/> : ""}

    </React.Fragment>
  );
};
