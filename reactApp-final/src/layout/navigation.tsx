import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
} from "reactstrap";
import { NavLink } from "react-router-dom";

const Navigation = () => {
  const [isOpen, setOpen] = useState<boolean>(false);

  const toggleNavigation = () => {
    setOpen(isOpen => !isOpen);
  };

  return (
    <div>
      <Navbar color="light" light expand="md" className="navigation">
        <NavbarBrand href="/">ITB1704</NavbarBrand>
        <NavbarToggler onClick={() => toggleNavigation()} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink className="nav-link" to="/">
                Home
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/student">
                Students
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/course">Courses</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/curriculum">
                Curriculums
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="nav-link" to="/person">
                People
              </NavLink>
            </NavItem>

          </Nav>

        </Collapse>
      </Navbar>
    </div>
  );
};

export default Navigation;
