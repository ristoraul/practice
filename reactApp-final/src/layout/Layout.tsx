import React from "react";
import Navigation from "./navigation";

const Layout: React.FC<{ children: any }> = ({ children }) => {
  return (
    <div className="layout">
      <Navigation />
      <div className="layout__main">{children}</div>
    </div>
  );
};

export default Layout;
